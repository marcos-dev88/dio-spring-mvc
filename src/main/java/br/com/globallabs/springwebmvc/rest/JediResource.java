package br.com.globallabs.springwebmvc.rest;

import br.com.globallabs.springwebmvc.model.Jedi;
import br.com.globallabs.springwebmvc.repository.JediRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
public class JediResource {

    @Autowired
    private JediRepository repository;

    @GetMapping("api/listJedi")
    public List<Jedi> getAllJedis(){
        return repository.findAll();
    }

    @GetMapping("api/listJedi/{id}")
    public Optional<Jedi> getjediById(@PathVariable("id") Long id){
        final Optional<Jedi> jedi = repository.findById(id);
        return jedi;
    }
}
