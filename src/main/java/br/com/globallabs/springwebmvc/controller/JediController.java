package br.com.globallabs.springwebmvc.controller;

import br.com.globallabs.springwebmvc.model.Jedi;
import br.com.globallabs.springwebmvc.repository.JediRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
public class JediController {    //Responsável pelo controle MVC das páginas HTML


    @Autowired //Com o Autowired, estou injetando uma instancia de uma classe repository na minha classe
    private JediRepository jediRepository;

    public JediController(JediRepository jediRepository) {
        this.jediRepository = jediRepository;
    }

    @GetMapping("/")
   public ModelAndView jedi(){
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("allJedi", jediRepository.findAll());
        return modelAndView;
    }

    @GetMapping("/newJedi")
    public ModelAndView addJedi(){
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("newJedi");
        modelAndView.addObject("jedi", new Jedi());

        return modelAndView;
    }

    @PostMapping("/")
    public String createJedi(@Valid  @ModelAttribute Jedi jedi, BindingResult result, RedirectAttributes redirect){

        if(result.hasErrors()){
            return "newJedi";
        }

        redirect.addFlashAttribute("message", "Jedi cadastrado com sucesso!");
        jediRepository.save(jedi);
        return "redirect:/";
    }

}
